import React, {Component} from "react";
import './Counter.css';
import {connect} from 'react-redux'
import {fetchCounter,updateCounter} from "../../store/actions";

class Counter extends Component {
    componentDidMount () {
        this.props.fetchCounter();
    }

    render() {
        return (
            <div className="Counter">
                <h1>{this.props.ctr}</h1>
                <button onClick={this.props.increaseCounter}>Increase1</button>
                <button onClick={this.props.decreaseCounter}>Decrease</button>
                <button  onClick={this.props.increaseByFive}>Increase by 5</button>
                <button  onClick={this.props.decreaseByFive}>Decrease by 5</button>
            </div>
        );
    }
}
const mapStateToProps = state => {
    return {
        ctr: state.counter,
    }
};
const mapDispatchToProps = dispatch => {
    return {
        fetchCounter: () => dispatch(fetchCounter()),
        increaseCounter: () => dispatch(updateCounter(1)),
        decreaseCounter: () => dispatch(updateCounter(-1)),
        increaseByFive: () => dispatch(updateCounter(5)),
        decreaseByFive: () => dispatch(updateCounter(-5))
    };

};

   export default connect(mapStateToProps,mapDispatchToProps)(Counter);