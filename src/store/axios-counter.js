import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://vik2405-ed905.firebaseio.com/'
});

export default instance;


