
import axios from './axios-counter'


export const FETCH_COUNTER_REQUEST = 'FETCH_COUNTER_REQUEST';
export const FETCH_COUNTER_SUCCESS = 'FETCH_COUNTER_SUCCESS';
export const FETCH_COUNTER_ERROR = 'FETCH_COUNTER_ERROR';

export const fetchCounterRequest = () => {
    return { type: FETCH_COUNTER_REQUEST };
};

export const fetchCounterSuccess = (counter) => {
    return { type: FETCH_COUNTER_SUCCESS, counter:counter};
};

export const fetchCounterError = () => {
    return { type: FETCH_COUNTER_ERROR };
};

export const fetchCounter = () => {
    return dispatch => {
        dispatch(fetchCounterRequest());
        axios.get('/counter.json').then(response => {
            dispatch(fetchCounterSuccess(response.data));
        }, error => {
            dispatch(fetchCounterError());
        });
    }
};

export const updateCounter = (amount) => {
    return (dispatch, getState) => {
        dispatch(fetchCounterRequest());
        axios.put('/counter.json', getState().counter + amount)
            .then(res => dispatch(fetchCounterSuccess(res.data)),
                    error => dispatch(fetchCounterError()))
    }
};

